**<span class="underline">Theme Development & Customization Using
Orchard Core CMS</span>**

**<span class="underline">Steps:</span>**

**<span class="underline">How to include Orchard Core in an empty web
project:</span>**

  - Create an empty .Net Core 3.0 web project and name it
    PersonalWebsiteApp:
    
    ![](images/media/image1.png)

  - Install
    <span class="underline">orchardcore.application.cms.targets</span>
    nuget package from manage nuget packages:
    
    ![](images/media/image2.png)

  - Open Startup.cs file & modify the ConfigureServices method by adding
    this line:
    
    services.AddOrchardCms();

  - In the Configure method, replace this block:
  
```csharp
    app.Run(async (context) =>
    {
    await context.Response.WriteAsync("Hello World!");
    });
```

  - with this line:
```csharp
    app.UseOrchardCore();
```
  - Launch PersonalWebsiteApp (Ctrl+F5). The setup page is displayed:
    
    ![](images/media/image3.png)

  - Enter the required information about the site:

<!-- end list -->

  > **The name of the site. Ex: Shirajul Islam.**

  > **The theme recipe to use. Ex: Agency.**

  > **The timezone of the site. Ex: (GMT+06) Asia/Dhaka (+06:00)**

  > **The Sql provider to use. Ex: SqlServer & provide the server and
    database information**

  > **The name of the admin user. Ex: Admin.**

  > **The email of the admin. Ex: <rajib.enlightsolutions@gmail.com>**

  > **Enter the password and the password confirmation.**

<!-- end list -->

  - And finally the site look like this:
    
    <span class="underline">HomePage:</span>
    
    ![](images/media/image4.png)
    
    <span class="underline">Admin Panel:</span>
    
    ![](images/media/image5.png)
    
    **<span class="underline">How to create a new theme:</span>**
    
    Steps:

  - In this solution add a new class library (.Net standard) & rename it
    as PersonalWebsiteApp.Theme :![](images/media/image6.png)

  - Install three nuget packages from mange nuget packages on the
    PersonalWebsiteApp.Theme project:

<!-- end list -->

1.  OrchardCore.Theme.Targets

2.  OrchardCore.ResourceManagement.Abstractions

3.  OrchardCore.DisplayManagement

<!-- end list -->

  - Add a new folder and name it wwwroot & store all static files in
    this folder:![](images/media/image7.png)

  - Add another folder rename it as Views & add *Layout.liquid*
    file:![](images/media/image8.png)

  - 
  - Now edit the Layout.liquid file:
    
    ![](images/media/image9.png)

  - For Orchard Core to identify this module it will now require
    a Manifest.cs file. Here is an example of that file:
```csharp    
    using OrchardCore.DisplayManagement.Manifest;
    
    [assembly: Theme(
    
    Name = "Personal Portfolio Theme",
    
    Author = "ENLIGHT SOLUTIONS",
    
    Website = "https://enlightsolution.com",
    
    Version = "2.0.0",
    
    Description = "This is personal portfolio theme template."
    
    )]
```

  - In PersonalWebsiteApp project add a
    reference:![](images/media/image10.png)

  - Now run (F5) this application & see the admin panel.Now we see our
    own custom theme template.![](images/media/image11.png)

  - Select Make Default from the admin panel and check the output we see
    our theme works.
